module.exports = function (grunt) {
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
	useminPrepare: 'grunt-usemin'
});

    grunt.initConfig({
        watch: {
            files: 'app/scss/**/*.scss',
            tasks: ['sass']
        },
        sass: {
            dev: {
                files: {
                    'app/css/main.css': 'app/scss/main.scss'
                }
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        'css/*.css',
                        '*.html',
			'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
			baseDir: './'
		    }
                }
            }
        },
	imagemin: {
	    dynamic: {
            files: [{
                expand: true,
                cwd: './',
                src: ['images/*.{png,jpg,gif}'],
                dest: 'dist/'
            }]
//            }
	}

	},
	copy:{
	    html:{
		files: [{
		    expand: true,
		    dot: true,
		    cwd: './',
		    src: ['*.html'],
		    dest: 'dist'
		}]
	    },
	},
	cssmin:{
	    dist: {}
	},
	clean:{
	    build: {
		src: ['dist/']
	    }
	},
	uglify: {
	    dist: {}
	},
	filerev: {
	    encoding: 'utf8',
	    algorithm: 20
	},
	release: {
	    files: [{
		src: [
		    'dist/js/*.js',
		    'dist/css/*.css',
		]
	    }]
	
	},
	concat: {
	    options: {
		separator: ';'
	    },
	    dist: {}
	},
	useminPrepare: {
	    foo: {
		dest: 'dist',
		src: ['index.html', 'contacto.html', 'nosotros.html', 'vosotros.html', 'ustedes.html', 'ellos.html']
	    
	},
	options: {
	    flow: {
		steps: {
		    css: ['cssmin'],
		    js: ['uglify']
		},
		post:{
		    css: [{
			name:'cssmin',
			createConfig: function(context, block){
			    var generated = context.options.generated;
			    generated.options={
				keepSpecialComments: 0,
				rebase: false
			    }
			}
		    }]
		}
	    }
	}
	},
  	usemin:{
  	    html: ['dist/index.html', 'dist/contacto.html', 'dist/nosotros.html', 'dist/vosotros.html', 'dist/ustedes.html', 'dist/ellos.html'],
	    options:{
		assetsDir:['dist', 'dist/css', 'dist/js']
	    }
  	}  

	
    });

// load npm tasks
 // grunt.loadNpmTasks('grunt-contrib-imagemin');
 // grunt.loadNpmTasks('grunt-contrib-sass');
 // grunt.loadNpmTasks('grunt-contrib-watch');
 // grunt.loadNpmTasks('grunt-browser-sync');

    // define default task
grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('img:compress', ['imagemin']);
    grunt.registerTask('build', [
	'clean',
	'copy',
	'imagemin',
	'useminPrepare',
	'concat',
	'uglify',
	'filerev',
	'usemin'
    ])
};
